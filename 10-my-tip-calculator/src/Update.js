import { MSGS } from './msgs';
import { computePayment } from './lib';

/**
 * Updates the model by producing a model with new/updated values.
 *
 * @param {object} msg
 * @param {object} model
 * @return {object}
 */
function update (msg, model) {
  const { type, payload } = msg;
  switch (type) {
    case MSGS.INPUT_AMOUNT: {
      return { ...model, inputAmount: msg.payload }
    }
    case MSGS.INPUT_PERCENT: {
      return { ...model, inputPercent: msg.payload }
    }
  }
  return model;
}

export default update;
