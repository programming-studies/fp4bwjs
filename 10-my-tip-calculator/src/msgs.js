/**
 * Message types.
 */
export const MSGS = {
  INPUT_AMOUNT: 'INPUT_AMOUNT',
  INPUT_PERCENT: 'INPUT_PERCENT',
}

/**
 * Produce an "input amount" message object.
 *
 * @param {number|string} payload
 * @return {object}
 */
export const msgInputAmount = payload => ({
  type: MSGS.INPUT_AMOUNT,
  payload,
});


/**
 * Produce an "input percent" message object.
 *
 * @param {number|string} payload
 * @return {object}
 */
export const msgInputPercent = payload => ({
  type: MSGS.INPUT_PERCENT,
  payload,
});
