/**
 * Initial model.
 *
 * Remember, input fields values are always strings. Users may type numbers,
 * but the data type on the inputs become strings. It is how the DOM and form
 * elements work.
 */
const initModel = {
  inputAmount: '',
  inputPercent: '',
};

export default initModel;
