
import {
  concat,
  curry,
  defaultTo,
  pipe,
} from 'ramda';


/**
 * Round a number to `places` decimal places.
 *
 * @param {number} places
 *  The number of decimal places to format the number with.
 * @return {number}
 */
const round = places => pipe(
  num => num * Math.pow(10, places),
  Math.round,
  num => num * Math.pow(10, -1 * places),
);


/**
 * Produce object with tip and total to pay.
 *
 * @param {number} billAmount
 * @param {number} tipPercent
 * @return {array[string, string]}
 *  An array where the first element is the value of the tip, and the
 *  second element is the total to pay.
 */
export const calcTipAndTotal = (billAmount, tipPercent) => {
  const bill = parseFloat(billAmount);
  const tip = bill * parseFloat(tipPercent) / 100 || 0;
  const total = bill + tip;
  return [tip, total];
}


/**
 * Formats money values.
 *
 * @param {string} symbol
 *  A symbol like the US Dollar ‘$’ or the Brazilian Real ‘R$’.
 * @param {number} places
 *  The number of decimal places to format the number with.
 * @param {number}
 *  The actual value to format.
 */
export const formatMoney = curry(
  (symbol, places, number) => pipe(
    defaultTo(0),
    round(places),
    num => num.toFixed(places),
    concat(symbol),
  )(number),
);
