import hh from 'hyperscript-helpers';
import { h } from 'virtual-dom';

const {
  div,
  h1,
  input,
  label,
  pre,
} = hh(h);

import {
  calcTipAndTotal,
  formatMoney,
} from './lib';

import {
  msgInputAmount,
  msgInputPercent,
} from './msgs';

/**
 * Produce the UI for a label with an input element.
 *
 * @param {string} name
 *  What to show as the description for the input field.
 * @param {number|string} value
 *  The value to show in the input field.
 * @param {function} oninput
 *  A handler for the `oninput` event.
 * @return {HTMLElement} with label and input children.
 */
const inputSet = (name, value, oninput) => {
  return div({ className: 'w-40' }, [
    label({ className: 'db fw6 lh-copy f5' }, name ),
    input({
      className: 'border-box pa2 ba mb2 tr w-100',
      value,
      oninput,
    }),
  ]);
};

/**
 * Produce the UI for the calculated tip and amount.
 *
 * @param {string} description
 *  The description of the value being shown.
 * @param {number|string} amount
 *  The value to show.
 * @return {HTMLElement}
 *  The HTML to display.
 */
const calculatedAmount = (description, amount) => {
  return div({ className: 'flex w-100' }, [
    div({ classname: 'w-50 pv1 pr2' }, description),
    div({ classname: 'w-50 tr pv1 pr2' }, amount),
  ]);
};

/**
 * Produce the UI to display the tip and total amount to play.
 *
 * @param {string|number} tip
 *  The tip amount to pay.
 * @param {string|number} total
 *  The total amount to pay.
 * @return {HTMLElement}
 *  The HTML to display.
 */
const calculatedAmounts = (tip, total) => {
  return div({ className: 'w-48 b bt bt2 pt2' }, [
    calculatedAmount('Tip:', tip),
    calculatedAmount('Total:', total),
  ]);
};

/**
 * The main View function.
 *
 * @param {function} dispatch
 *  The function that dispatches messages with payload.
 * @param {object} model
 *  The model object this application uses.
 * @return {HTMLElement}
 *  The entire's app's view to display.
 */
function view(dispatch, model) {

  const { inputAmount, inputPercent } = model;

  const [tip, total] = calcTipAndTotal(inputAmount, inputPercent);

  const toMoney = formatMoney('$', 2);

  return div({ className: 'mw6 center' }, [
    h1({ className: 'f2 pv2 bb' }, 'Tip Calculator'),
    inputSet('Bill Amount', inputAmount, evt =>
      dispatch(msgInputAmount(evt.target.value)),
    ),
    inputSet('Tip %', inputPercent, evt =>
      dispatch(msgInputPercent(evt.target.value)),
    ),
    calculatedAmounts(toMoney(tip), toMoney(total)),
  ]);
}

export default view;
