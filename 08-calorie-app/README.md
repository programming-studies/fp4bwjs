# Plan and Data Model

- [data model](#data-model)
- [view functions](#view-functions)


## data model

```js
const meal = {
  id: 1,
  description: 'Breakfast',
  calories: 460,
};

const model = {
  meals: [meal1, meal2],
  showForm: false,

  // To show on the description input field as the placeholder.
  description: 'Dinner',

  // To show on the calories input field as the placeholder.
  calories: 600,

  editId: 3,
  nextId: 4
};
```

## view functions

```
view
  formView
  