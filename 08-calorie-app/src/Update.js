import {
  __ as _,
  curry,
  defaultTo,
  filter,
  find,
  map,
  pipe,
} from 'ramda';

const MSGS = {
  SHOW_FORM: 'SHOW_FORM',
  MEAL_INPUT: 'MEAL_INPUT',
  CALORIES_INPUT: 'CALORIES_INPUT',
  SAVE_MEAL: 'SAVE_MEAL',
  DELETE_MEAL: 'DELETE_MEAL',
  EDIT_MEAL: 'EDIT_MEAL',
};

export const msgShowForm = showForm => ({
  type: MSGS.SHOW_FORM,
  showForm,
});

export const msgMealInput = description => ({
  type: MSGS.MEAL_INPUT,
  description,
});

export const msgCaloriesInput = calories => ({
  type: MSGS.CALORIES_INPUT,
  calories,
});

//
// No need for a `msgSaveMeal` function because there is no payload
// for the message. We only need a constant.
//
export const msgSaveMeal = { type: MSGS.SAVE_MEAL };

export const msgDeleteMeal = id => ({
  type: MSGS.DELETE_MEAL,
  id
});

export const msgEditMeal = editId => ({
  type: MSGS.EDIT_MEAL,
  editId,
});

const add = (msg, model) => {
  const { nextId, description, calories } = model;
  const meal = { id: nextId, description, calories };
  const meals = [ ...model.meals, meal ];
  return {
    ...model,
    meals,
    nextId: nextId + 1,
    description: '',
    calories: 0,
    showForm: false,
  };
};

const edit = (msg, model) => {
  const { editId, description, calories } = model;
  const meals = map(meal => {
    if (meal.id === editId) {
      return { ...meal, description, calories };
    }
    return meal;
  }, model.meals);

  return {
    ...model,
    meals,
    description: '',
    calories: 0,
    showForm: false,
    editId: null,
  };
};

const update = (msg, model) => {
  switch (msg.type) {
    case MSGS.SHOW_FORM: {
      const { showForm } = msg;
      return { ...model, showForm, description: '', calories: 0 };
    }
    case MSGS.MEAL_INPUT: {
      const { description } = msg;
      return { ...model, description };
    }
    case MSGS.CALORIES_INPUT: {
      const calories = pipe(
        curry(parseInt)(_, 10),
        defaultTo(0),
      )(msg.calories);
      return { ...model, calories };
    }
    case MSGS.SAVE_MEAL: {
      const { editId } = model;
      const updatedModel = editId !== null
        ? edit(msg, model)
        : add(msg, model);
      return updatedModel;
    }
    case MSGS.DELETE_MEAL: {
      const { id } = msg;
      const meals = filter(meal => meal.id !== id, model.meals);
      return { ...model, meals };
    }
    case MSGS.EDIT_MEAL: {
      const { editId } = msg;
      const meal = find(meal => meal.id === editId, model.meals);
      const { description, calories } = meal;
      return {
        ...model,
        editId,
        description,
        calories,
        showForm: true,
      };
    }
  }
  return model;
};

export default update;

