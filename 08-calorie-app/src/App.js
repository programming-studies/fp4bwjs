import { h, diff, patch } from 'virtual-dom';
import createElement from 'virtual-dom/create-element';

//
// BEWARE! IMPURE FUNCTIONS BELOW. Use them at your own risk.
//

const app = (initModel, update, view, node) => {
  let model = {
    ...initModel,
    meals: [
      { id: 1, description: 'Afternoon Snack', calories: 128 },
    ],
  };
  let currentView = view(dispatch, model);

  let rootNode = createElement(currentView);

  node.appendChild(rootNode);

  function dispatch(msg) {
    model = update(msg, model);
    const updatedView = view(dispatch, model);
    const patches = diff(currentView, updatedView);
    rootNode = patch(rootNode, patches);
    currentView = updatedView;
  }
};

export default app;

