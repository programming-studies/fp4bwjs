import hh from 'hyperscript-helpers';
import { h } from 'virtual-dom';

import {
  append,
  head,
  last,
  map,
  partial,
  reduce,
} from 'ramda';

const {
  button,
  div,
  form,
  h1,
  i,
  input,
  label,
  pre,
  table,
  thead,
  tbody,
  tr,
  th,
  td,
} = hh(h);

import {
  msgShowForm,
  msgMealInput,
  msgCaloriesInput,
  msgSaveMeal,
  msgDeleteMeal,
  msgEditMeal,
} from './Update';

const fieldSet = (labelText, inputValue, oninput) => {
  return div([
    label({ className: 'db mb1'}, labelText),
    input({
      className: 'pa2 input-reset ba w-100 mb2',
      type: 'text',
      value: inputValue,
      oninput,
    }),
  ]);
};

const buttonSet = (dispatch) => {
  return div([
    button(
      {
        className: 'f3 pv2 ph3 bg-blue white bn mr2 dim',
        type: 'submit',
      },
      'Save',
    ),
    button(
      {
        className: 'f3 pv2 ph3 bn bg-light-gray dim',
        type: 'button',
        onclick: () => dispatch(msgShowForm(false)),
      },
      'Cancel',
    ),
  ]);
};

const formView = (dispatch, model) => {
  const { description, calories, showForm } = model;
  if (showForm) {
    return form(
      {
        className: 'w-100 mv2',
        onsubmit: evt => {
          evt.preventDefault();
          dispatch(msgSaveMeal);
        }
      },
      [
        fieldSet('Meal', description,
          evt => dispatch(msgMealInput(evt.target.value))
        ),
        fieldSet('Calories', calories || '',
          evt => dispatch(msgCaloriesInput(evt.target.value))
        ),
        buttonSet(dispatch),
      ],
    );
  }
  return button(
    {
      className: 'f3 pv2 ph3 bg-blue white bn',
      onclick: () => dispatch(msgShowForm(true)),
    },
    'Add Meal'
  );
};

const cell = (tag, className, value) => tag({ className }, value);

// A simple constant, not a function.
const tableHeader = thead(
  tr([
    cell(th, 'pa2 tl', 'Meal'),
    cell(th, 'pa2 tr', 'Calories'),
    cell(th, '', 'Actions'),
  ]),
);

const mealRow = (dispatch, className = '', meal) => {
  console.log('meal', meal);
  return tr({ className }, [
    cell(td, 'pa2 tl', meal.description),
    cell(td, 'pa2 tr', meal.calories),
    cell(td, 'pa2 tr', [
      i({
        className: 'ph1 pointer fa fa-trash-o',
        onclick: () => dispatch(msgDeleteMeal(meal.id)),
      }),
      i({
        className: 'ph1 pointer fa fa-pencil-square-o',
        onclick: () => dispatch(msgEditMeal(meal.id)),
      }),
    ]),
  ]);
};

const totalRow = meals => {
  const total = reduce((acc, { calories }) => acc + calories, 0, meals);
  return tr({ className: 'bt b' }, [
    cell(td, 'pa2 tr', 'Total'),
    cell(td, 'pa2 tr', total),
  ]);
};

const mealsBody = (dispatch, className, meals) => {
  const rows = map(partial(mealRow, [dispatch, 'stripe-dark']), meals);
  const rowsWithTotal = [...rows, totalRow(meals)];
  return tbody({ className }, rowsWithTotal);
};

const tableView = (dispatch, model) => {
  if (model.meals.length === 0) {
    return div(
      { className: 'mv2 i black-50' },
      'No meals to display...',
    );
  }

  return table({ className: 'center w-100 collapase' }, [
    tableHeader,
    mealsBody(dispatch, '', model.meals),
  ]);
};

const view = (dispatch, model) => {
  return div({ className: 'mw6 center' }, [
    h1({ className: 'f2 pv2 bb' }, 'Calorie Counter'),
    formView(dispatch, model),
    tableView(dispatch, model),
    pre(JSON.stringify(model, null, 2)),
  ]);
};

export default view;

