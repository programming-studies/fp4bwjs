const { td, th, tr, table, thead, tbody } = tags;
const { append, map, partial, reduce } = R;

const log = console.log.bind(console);

// Meal { description: String, calories: Number }

const meals = [
  { description: 'Breakfast', calories: 460 },
  { description: 'Snack', calories: 180 },
  { description: 'Lunch', calories: 600 },
];

/**
 * cell :: fn string string
 */
const cell = (tag, className, value) => {
  return tag({ className }, value);
};

/**
 * mealRow :: string object: {description: string, calories: number}
 */
const mealRow = (className, meal) => {
  return tr({ className }, [
    cell(td, 'pa2 tl', meal.description),
    cell(td, 'pa2 tr', meal.calories)
  ]);
};

/**
 * mealBody :: string [object]
 */
const mealBody = (className, meals) => {
  const rows = map(partial(mealRow, ['stripe-dark']), meals); // <1>
  const rowsWithTotal = append(totalRow(meals), rows);
  return tbody({ className }, rowsWithTotal);
};


/**
 * headerRow :: [string] -> HTMLElement
 */
const headerRow = (columnTitles) => {
  return tr([
    cell(th, 'pa2 tl', columnTitles[0]),
    cell(th, 'pa2 tr', columnTitles[1]),
  ]);
};

/**
 * mealHeader :: [string]
 */
const mealHeader = (columnTitles) => {
  return thead(headerRow(['Meal', 'Calories']));
};

 /**
  * totalRow :: [Meal]
  */
 const totalRow = (meals) => {
   const totalCalories = reduce((acc, { calories }) => acc + calories, 0, meals);
   return tr({ className: 'bt b' }, [
     cell(td, 'pa2 tr', 'Total'),
     cell(td, 'pa2 tr', totalCalories),
   ]);
 };

/**
 * mealsTable
 */
const mealsTable = meals => {
  return table({ className: 'mw5 center w-100 collapse' }, [
    mealHeader(),
    mealBody('', meals),
  ]);
};

// const view = cell(td, 'pa2', 'Lunch');
// const view = mealHeader('', ['Meal', 'Calories']);
// const view = totalRow('', meals);
const view = mealsTable(meals);

app.appendChild(view);

//
// 1. `map` takes a function that expects one param. `mealRow` (passed to map)
// expects two params. So, we partially apply `mealRow` so that we pass it one
// param, which returns a function that expects one param, which makes `map` glad.
//
