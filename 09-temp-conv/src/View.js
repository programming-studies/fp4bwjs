import hh from 'hyperscript-helpers';
import { h } from 'virtual-dom';
import {
  identity,
  map,
  pipe,
} from 'ramda';

const {
  div,
  h1,
  input,
  select,
  option,
  pre,
} = hh(h);

import {
  msgLeftValueInput,
  msgRightValueInput,
  msgLeftUnitChanged,
  msgRightUnitChanged,
} from './Update';


const UNITS = ['Fahrenheit', 'Celsius', 'Kelvin'];

// Not pure. Uses UNITS from outside... not easy to test, etc, etc...
const unitOptions = (selectedUnit) => {
  return map(
    unit => option({ value: unit, selected: selectedUnit === unit }, unit),
    UNITS,
  );
}

const unitSection = (dispatch, unit, value, msgFn, unitFn) => {
  return div({ className: 'w-50 ma1' }, [
    input({
      className: 'db w-100 mv2 pa2 input-reset ba',
      type: 'text',
      value,
      oninput: evt => dispatch(msgFn(evt.target.value)),
    }),
    select(
      {
        className: 'db w-100 pa2 ba input-reset br1 bg-white ba b--black',
        onchange: evt => dispatch(unitFn(evt.target.value)),
      },
      unitOptions(unit),
    ),
  ]);
};

function view(dispatch, model) {
  return div({ className: 'mw6 center' }, [
    h1({ className: 'f2 pv2 bb' }, 'Temperature Unit Converter'),
    div({ className: 'flex' }, [
      unitSection(
        dispatch,
        model.leftUnit,
        model.leftValue,
        msgLeftValueInput,
        msgLeftUnitChanged,
      ),
      unitSection(
        dispatch,
        model.rightUnit,
        model.rightValue,
        msgRightValueInput,
        msgRightUnitChanged,
      ),
    ]),
    pre(JSON.stringify(model, null, 2)),
  ]);
}

export default view;

