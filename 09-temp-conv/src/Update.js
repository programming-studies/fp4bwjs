import {
  defaultTo,
  identity,
  pathOr,
  pipe,
} from 'ramda';

export const MSGS = {
  LEFT_VALUE_INPUT: 'LEFT_VALUE_INPUT',
  RIGHT_VALUE_INPUT: 'RIGHT_VALUE_INPUT',
  LEFT_UNIT_CHANGED: 'LEFT_UNIT_CHANGED',
  RIGHT_UNIT_CHANGED: 'RIGHT_UNIT_CHANGED',
};

export const msgLeftValueInput = leftValue => ({
  type: MSGS.LEFT_VALUE_INPUT,
  leftValue,
});

export const msgRightValueInput = rightValue => ({
  type: MSGS.RIGHT_VALUE_INPUT,
  rightValue,
});

export const msgLeftUnitChanged = leftUnit => ({
  type: MSGS.LEFT_UNIT_CHANGED,
  leftUnit,
});

export const msgRightUnitChanged = rightUnit => ({
  type: MSGS.RIGHT_UNIT_CHANGED,
  rightUnit,
});

const toInt = pipe(parseInt, defaultTo(0));

const round = pipe(Math.round, defaultTo(0));

const f2c = temp => 5 / 9 * (temp - 32);

const c2f = temp => 9 / 5 * temp + 32;

const k2c = temp => temp - 273.15;

const c2k = temp => temp + 273.15;

const f2k = pipe(f2c, c2k);

const k2f = pipe(k2c, c2f);

const UnitConversions = {
  Celsius: {
    Fahrenheit: c2f,
    Kelvin: c2k,
  },
  Fahrenheit: {
    Celsius: f2c,
    Kelvin: f2k,
  },
  Kelvin: {
    Fahrenheit: k2f,
    Celsius: k2c,
  },
};

const convertFromToTemp = (fromUnit, toUnit, temp) => {
  const convertFn = pathOr(
    identity,
    [fromUnit, toUnit],
    UnitConversions,
  )
  return convertFn(temp);
};

const convert = (model) => {
  const { leftValue, leftUnit, rightValue, rightUnit, sourceLeft } = model;

  const [fromUnit, fromTemp, toUnit] = sourceLeft
    ? [leftUnit, leftValue, rightUnit]
    : [rightUnit, rightValue, leftUnit];

  const otherValue = pipe(
    convertFromToTemp,
    round,
  )(fromUnit, toUnit, fromTemp);

  return sourceLeft
    ? { ...model, rightValue: otherValue }
    : { ...model, leftValue: otherValue };
};

const update = (msg, model) => {
  switch (msg.type) {
    case MSGS.LEFT_VALUE_INPUT: {
      if (msg.leftValue === '') {
        return { ...model, sourceLeft: true, leftValue: '', rightValue: '' };
      }
      const leftValue = toInt(msg.leftValue);
      return convert({ ... model, sourceLeft: true, leftValue });
    }
    case MSGS.RIGHT_VALUE_INPUT: {
      if (msg.rightValue === '') {
        return { ...model, sourceLeft: false, leftValue: '', rightValue: '' };
      }
      const rightValue = toInt(msg.rightValue);
      return convert({ ... model, sourceLeft: false, rightValue });
    }
    case MSGS.LEFT_UNIT_CHANGED: {
      return { ...model, leftUnit: msg.leftUnit };
    }
    case MSGS.RIGHT_UNIT_CHANGED: {
      return convert({ ...model, rightUnit: msg.rightUnit });
    }
  }
  return model;
}

export default update;

