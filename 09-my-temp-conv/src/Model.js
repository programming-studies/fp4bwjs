import { UNITS } from './lib';

const initModel = {
  inputLeft: '',
  unitLeft: UNITS[1],
  inputRight: '',
  unitRight: UNITS[0],
  /**
   * If the user is entering data on the left panel input, then we update the
   * right panel input, and vice-versa.
   */
  dataSource: 'left' // 'left', 'right'
};

export default initModel;

