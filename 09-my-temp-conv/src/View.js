import hh from 'hyperscript-helpers';
import { h } from 'virtual-dom';

import {
  map,
} from 'ramda';

const {
  div,
  h1,
  input,
  option,
  pre,
  select,
} = hh(h);

import { UNITS } from './lib';

import {
  msgInputLeft,
  msgInputRight,
} from './Update';

const unitOptions = (selectedUnit, units) => {
  return map(
    unit => option({ value: unit, selected: selectedUnit === unit }, unit),
    units,
  );
};

const vSection = (dispatch, msgFn, unit, value) => {
  return div({ className: 'w-50 ma1' }, [
    input({
      className: 'db w-100 mv2 pa2 input-reset ba',
      type: 'text',
      value,
      oninput: evt => dispatch(msgFn(evt.target.value)),
    }),
    select(
      {
        className: 'db w-100 pa2 ba input-reset br1 bg-white ba b--black',
      },
      unitOptions(unit, UNITS),
    ),
  ]);
};

function view(dispatch, model) {
  const { inputLeft, inputRight, unitLeft, unitRight } = model;

  return div({ className: 'mw6 center' }, [
    h1({ className: 'f2 pv2 bb' }, 'Temperature Unit Converter'),
    div({ className: 'flex' }, [
      vSection(dispatch, msgInputLeft, unitLeft, inputLeft),
      vSection(dispatch, msgInputRight, unitRight, inputRight),
    ]),

    pre(JSON.stringify(model, null, 2)),
  ]);
}

export default view;
