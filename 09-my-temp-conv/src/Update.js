
import { convert } from './lib';

const MSGS = {
  INPUT_LEFT: 'INPUT_LEFT',
  INPUT_RIGHT: 'INPUT_RIGHT',
  UNIT_LEFT: 'UNIT_LEFT',
  UNIT_RIGHT: 'UNIT_RIGHT',
};

export const msgInputLeft = value => ({
  type: MSGS.INPUT_LEFT,
  value,
});

export const msgInputRight = value => ({
  type: MSGS.INPUT_RIGHT,
  value,
});

function update (msg, model) {
  const { type, value } = msg;
  switch (type) {
    case MSGS.INPUT_LEFT: {
      if (value === '') {
        return { ...model, dataSource: 'left', inputLeft: '', inputRight: '' };
      }
      return convert({ ...model, dataSource: 'left', inputLeft: value });
    }
    case MSGS.INPUT_RIGHT: {
      if (value === '') {
        return { ...model, dataSource: 'right', inputLeft: '', inputRight: '' };
      }
      return convert({ ...model, dataSource: 'right', inputRight: value });
    }
  }
  return model;
}

export default update;

