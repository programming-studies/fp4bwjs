import {
  defaultTo,
  identity,
  pathOr,
  pipe,
} from 'ramda';

export const UNITS = ['Fahrenheit', 'Celsius', 'Kelvin'];

const toInt = pipe(parseInt, defaultTo(0));

const round = pipe(Math.round, defaultTo(0));

const f2c = temp => 5 / 9 * (temp - 32);

const c2f = temp => 9 / 5 * temp + 32;

const k2c = temp => temp - 273.15;

const c2k = temp => temp + 273.15;

const f2k = pipe(f2c, c2k);

const k2f = pipe(k2c, c2f);

export const UnitConversions = {
  Celsius: {
    Fahrenheit: c2f,
    Kelvin: c2k,
  },
  Fahrenheit: {
    Celsius: f2c,
    Kelvin: f2k,
  },
  Kelvin: {
    Fahrenheit: k2f,
    Celsius: k2c,
  },
};

const convertFromToTemp = (unitFrom, unitTo, temperature) => {
  const convertFn = pathOr(
    identity,
    [unitFrom, unitTo],
    UnitConversions
  );
  return convertFn(temperature);
};

export const convert = model => {
  const { inputLeft, unitLeft, inputRight, unitRight, dataSource } = model;

  const [unitFrom, valueFrom, unitTo] = dataSource === 'left'
    ? [unitLeft, inputLeft, unitRight]
    : [unitRight, inputRight, unitLeft];

  const otherValue = pipe(
    convertFromToTemp,
    round,
  )(unitFrom, unitTo, valueFrom);

  return dataSource === 'left'
    ? { ...model, inputRight: otherValue }
    : { ...model, inputLeft: otherValue };
};

