# Immutable Arrays

- [meals example](#meals-example)
  - [exercises](#exercises)
- [letter grade example](#letter-grade-example)
- [exercise reviews count](#exercise-reviews-count)
  - [my solution 1](#my-solution-1)
  - [book solution](#book-solution)



```js
const log = console.log.bind(console);
```

```js
const meals = [
  { id: 1, descr: 'Breakfast', calories: 420 },
  { id: 2, descr: 'Lunch', calories: 520 },
];

const meal = {
  id: 3,
  descr: 'Snack',
  calories: 180,
};

// Createa a new array using the original `meals` array and adding a new meal
// at the end.
const updatedMeals = [...meals, meal];
// log(updatedMeals);

// How to createa new array of meals and update the description of meal id 2 in
// the process?

const updateDescr = (meal) => {
  if (meal.id === 2) {
    return {
      ...meal,
      descr: 'Early Lunch',
    };
  }
  return meal;
};

const updatedMealsDescr = updatedMeals.map(updateDescr);

log(updateDescr(updatedMealsDescr));
```

## Exercises

1) create a constant named friends, which is an array that contains 3 names of your choosing.

```js
var friends1 = ['Anakin Skywalker', 'Luke Skywalker', 'Ahsoka Tano'];
```

2). Create a new constant named updatedFriends, which includes the friends array values plus one additional name

```js
var friends2 = [...friends1, 'Obi-wan Kenobi'];
log(friends2);
```

3). Create a new constant named friendNameLengths, which is based on the array updatedFriends, but instead of having the friends names, have the array store the length of each persons name.

```js
var nameLengths = friends2.map(friend => friend.length);
var longestLength = Math.max(...nameLengths);
```

4) Create a new constant named shorterNamedFriends, which will be a list of the friends except the friend with the longest name.

```js
var lengthPredicate = maxlen => name => name.length < maxlen;
var shortNames = friends2.filter(predicate(longestLength));
log(shortNames);
```

NOTE: I thought it better to create a curried predicate function instead of making filter use a function from the outer scope, which would make it impure.

## reduce

const grades = [60, 55, 80, 90, 99, 92, 75, 72];

const sum = (acc, val) => {
  return acc + val;
};

const total = grades.reduce(sum);

## letter grade example

```js
const groupByGrade = (acc, grade) => {
  const { a = 0, b = 0, c = 0, d = 0, f = 0 } = acc;

  if (grade >= 90) {
    return { ...acc, a: a + 1 };
  }
  else if (grade >= 80) {
    return { ...acc, b: b + 1 };
  }
  else if (grade >= 70) {
    return { ...acc, c: c + 1 };
  }
  else if (grade >= 60) {
    return { ...acc, d: d + 1 };
  }
  else {
    return { ...acc, f: f + 1 };
  }
};

const letterGradeCount = grades.reduce(groupByGrade, {});
log(letterGradeCount);

// [object Object] {
//   a: 3,
//   b: 1,
//   c: 2,
//   d: 1,
//   f: 1
// }
```

## exercise reviews count

const reviews = [4.5, 4.0, 5.0, 2.0, 1.0, 5.0, 3.0, 4.0, 1.0, 5.0, 4.5, 3.0, 2.5, 2.0];

Using the reduce function, create an object that has properties for each review value, where the value of the property is the number of reviews with that score. for example, the answer should be shaped like this:

```
{ 4.5: 1, 4.0: 2 ...}
```

TIP: checkout computed properties discussed [here]( https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Object_initializer#Computed_property_names).

solution can be found at:

https://jsbin.com/himuzuw/1/edit?js,console

This is my solution:
### my solution 1

```js
const groupByReview = (acc, review) => {
  if (acc[review]) {
    acc[review] += 1;
  }
  else {
    acc[review] = 1;
  }
  return acc;
};

const reviewCount = reviews.reduce(groupByReview, {});
log(reviewCount);
// { '1': 2, '2': 2, '3': 2, '4': 2, '5': 3, '4.5': 2, '2.5': 1 }
```

{ '1': 2, '2': 2, '3': 2, '4': 2, '5': 3, '4.5': 2, '2.5': 1 }

### book solution

```js
const reviews = [4.5, 4.0, 5.0, 2.0, 1.0, 5.0, 3.0, 4.0, 1.0, 5.0, 4.5, 3.0, 2.5, 2.0];

const groupBy = (acc, review) => {
  const count = acc[review] || 0;
  return { ...acc, [review]: count + 1 };
};

const countByReviews = reviews.reduce(groupBy, {});

console.log(countByReviews);
// { '1': 2, '2': 2, '3': 2, '4': 2, '5': 3, '4.5': 2, '2.5': 1 }
```
