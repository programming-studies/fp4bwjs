const log = console.log.bind(console);

// create the code to go from studentGrades array,
// to studentFeedback (as shown in comments below)

const studentGrades = [
  {name: 'Joe', grade: 88},
  {name: 'Jen', grade: 94},
  {name: 'Steph', grade: 77},
  {name: 'Allen', grade: 60},
  {name: 'Gina', grade: 54},
];

/*
const studentFeedback = [
  'Nice Job Joe, you got an b',
  'Excellent Job Jen, you got an a',
  'Well done Steph, you got an c',
  'What happened Allen, you got an d',
  'Not good Gina, you got an f',
];
*/

// Solution found at:
// https://jsbin.com/vaqomiy/1/edit?js,console

// const makeFeedback = ({ name, grade }) => {
//   if (grade >= 90) {
//     return `Excellent job ${name}, you got an a`;
//   }
//   else if (grade >= 80) {
//     return `Nice job ${name}, you got an b`;
//   }
//   else if (grade >= 70) {
//     return `Well done ${name}, you got an c`;
//   }
//   else if (grade >= 60) {
//     return `What happened ${name}, you got an d`;
//   }
//   else {
//     return `Not good ${name}, you got an f`;
//   }
// };

// Solution 2 which uses currying for not real good reason.
// const makeFeedback = msg => ({ name, grade }) => {
//   if (grade >= 90) {
//     return `Excellent job ${name}, ${msg} a`;
//   }
//   else if (grade >= 80) {
//     return `Nice job ${name}, ${msg} b`;
//   }
//   else if (grade >= 70) {
//     return `Well done ${name}, ${msg} c`;
//   }
//   else if (grade >= 60) {
//     return `What happened ${name}, ${msg} d`;
//   }
//   else {
//     return `Not good ${name}, ${msg} f`;
//   }
// };

const messages = {
  a: 'Excellent Job',
  b: 'Nice Job',
  c: 'Well done',
  d: 'What happened',
  f: 'Not good',
};

const letterGrade = points => {
  if (points >= 90) {
    return 'a';
  } else if (points >= 80) {
    return 'b';
  } else if (points >= 70) {
    return 'c';
  } else if (points >= 60) {
    return 'd';
  } else {
    return 'f';
  }
};

const makeFeedback = feedbackRules => ({ name, grade }) => {
  const letter = letterGrade(grade);
  const msg = feedbackRules[letter];
  return `${msg} ${name}, you got an ${letter}`;
}

const studentFeedback = studentGrades.map(makeFeedback(messages), []);
log(studentFeedback);
