const log = console.log.bind(console);

// const greet = (greeting, name) => `${greeting} ${name}`;
// log('May the force be with you', 'Master Yoda');

const greet = greeting => name => `${greeting} ${name}`;

// `greet` takes a greeting as parameter, and returns a function that takes a
// name as parameter. The returned function remembers the greeting parameter.

// Create a list of greetings from a list of names.

const friends = ['Yoda', 'Luke', 'Leia', 'Obi-wan'];

// Which tool should I use to transform an array of names into an array of
// greetings? MAP for the win!

// `map` takes only one parameter, but we need to pass it a name and a greeting
// message. That is one of the cases when currying helps. We call a function that
// takes an initial parameter, remembers it, and returns a function that then takes
// the parameter passed automatically by `map` on each iteration.

const friendGreetings = friends.map(greet('Hello there, '));
log(friendGreetings);
// [ 'Hello there,  Yoda',
//   'Hello there,  Luke',
//   'Hello there,  Leia',
//   'Hello there,  Obi-wan' ]

// `greet` is a high order function because it returns a function. `map` is a
// high order function because it takes a function as a parameter. The
// `greeting` parameter used in the returned function causes a _closure_ to be
// created.
