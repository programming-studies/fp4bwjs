const log = console.log.bind(console);

// Partial application is to call a function with less arguments than it needs
// to really return the final (non-function) value.

// PARTIAL APPLICATION: generalize a more general function.

//
// CURRYING is what you do to a function, how you design it. Not data is used at
// this point.
//
// PARTIAL APPLICATION is something that happens when you use the function, when
// you apply _some_ of the parameters (but not all of them), which causes the
// application to return another function that waits for the remaining arguments
// rather than the final value. This is when you actually use data.
//
// Partial application is what you are doing when you are using a curried
// function. A curried function may be partially applied (passed less params) or
// "fully" applied, when we pass all the params it needs at once.
//

// A curried greet function.
const greet = greeting => name => `${greeting} ${name}`;

// Partially apply greet (but hey, `greet` does take only one param...).
const morningGreet = greet('Good morning');
const afternoonGreet = greet('Good afternoon');

log(morningGreet('Obi-wan Kenobi'));
// Good morning Obi-wan Kenobi

log(afternoonGreet('Obi-wan Kenobi'));
// Good afternoon Obi-wan Kenobi

//
// BEWARE: The functions above cannot are curried in a more primitive, basic
// way. We must call `greet` with one arg, which will in turn return another
// function. Even if you pass two args to `greet`, it will still return a
// function that will have to be called later. It is not an “intelligent
// currying“ that detects that we passed all the arguments. Take a look at
//
//   https://ramdajs.com/docs/#curry
//
// Ramda's `curry` allows us to create true curried functions.
//

//
// When working with curried functions, the orther of the parameters matter.
// Define your curried functions to take the more generalized parameters first,
// and the more specialized ones last.
//

const { curry } = require('ramda');

// This is truly curried.
const greet = curry((greeting, name) => `${greeting} ${name}`);

// Invoke `greet` with all params at once.
const msg1 = greet('Hello', 'Yoda')
log(msg1);
// Hello Yoda

// Invoke `greet` with the greeting param only, which returns a function that
// expects one more param, which we call with the name Yoda again, which returns
// the final value/string.
const msg2 = greet('Hello')('Yoda');
log(msg2);
// Hello Yoda

// Or we could also do this. `sayHelloTo` is a function that takes the remaining
// `name` argument.
const sayHelloTo = greet('Hello');
const msg3 = sayHelloTo('Yoda');
log(msg3);
// Hello Yoda

