const { compose, filter, length, map, match, pipe, split } = require('ramda');
const log = console.log.bind(console);

// Pure functions - no side effects

// Impure functions (procedures) - side effects.

// Pure functions are:
// - reusable
// - composable
// - easy to test
// - easy to cache

// Function composition is the act of creating new functions by combining the
// logic of other functions.

const str = 'lorem ipsum dolor sit amet, mussum ipsum cacildis';

// Traditional, common approach.
const wordList = split(' ', str);
log(length(wordList));
// 8

// Manual composition by nesting invocations.
log(length(split(' ', str)));
// 8

// This is the real FP Composition Shit!
const countWords = compose(length, split);
log(countWords(' ', str));

// We could also partial apply `split` passing it the first parameter.
const count = compose(length, split(' '));
log(count(str));

//
// BEWARE: `compose` performs right-to-left function composition. The rightmost
// function may have any arity; the remaining functions must be unary.
//


// EXERCISE
// Count how many digits there are in the following
// sentence, using functional composition

// NOTE: If you get stuck, you can get some hints from
// the following jsbin:
// https://jsbin.com/jokefus/2/edit?js,console
// my solution is here: https://jsbin.com/duxewec/1/edit?js,console

const sentence = 'PechaKucha is a presentation style in which 20 slides are shown for 20 seconds each (6 minutes and 40 seconds in total).';

// My solution.
const numbersInString = pipe(
  match(/\d/g),
  length,
);

// There should be 7 digits on the sentence.

log(numbersInString(sentence));
// 7.

// NOTE: `match` returns an array with the matching elements if we pass the `g`
// modifier. Without `g`, it returns an array other sort of info about the
// (possibly) matching element.


// Solution from the course.
const numbersInString2 = pipe(
  split(''),
  map(parseInt),
  filter(Number.isInteger),
  length,
);

log(numbersInString2(sentence));
// 7.
